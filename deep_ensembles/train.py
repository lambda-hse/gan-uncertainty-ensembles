import torch
import numpy as np
import os
import matplotlib.pyplot as plt
from .params import *
from itertools import repeat
from IPython.display import clear_output
from tqdm import tqdm_notebook, tqdm
from torch.autograd import Variable, grad

device = 'cuda' if torch.cuda.is_available() else 'cpu'


class ParticleSet(torch.utils.data.Dataset):
    def __init__(self, data):
        self.data = np.array(data)

    def __len__(self):
        return self.data.shape[0]

    def __getitem__(self, ind):
        return (self.data[ind, y_count:-1],  # X
                self.data[ind, -1],  # Weight
                self.data[ind, :y_count],  # DLL
                )


def repeater(data_loader):
    for loader in repeat(data_loader):
        for data in loader:
            yield data


def calc_gradient_penalty(netC, real_data, fake_data, fake_data2, batch_size=32):
    alpha = torch.rand(batch_size, 1)
    alpha = alpha.expand(real_data.size())
    alpha = alpha.to(device)

    interpolates = alpha * real_data + ((1 - alpha) * fake_data)
    disc_interpolates = cramer_critic(netC, interpolates, fake_data2)

    gradients = grad(outputs=disc_interpolates, inputs=interpolates,
                     grad_outputs=torch.ones(disc_interpolates.size()).to(device),
                     create_graph=True, retain_graph=True, only_inputs=True)[0]
    slopes = gradients.norm(2, dim=1)
    gradient_penalty = ((torch.max(torch.abs(slopes) - 1, torch.zeros_like(slopes))) ** 2).mean()
    return gradient_penalty


def cramer_critic(netC, x, y):
    discriminated_x = netC(x)
    return torch.norm(discriminated_x - netC(y), dim=1) - torch.norm(discriminated_x, dim=1)


def ensemble_penalty(netC, x, y):
    return torch.norm(netC(x) - netC(y), dim=1)


def get_set_predictions(x, netGs, models_num=10, verbose=True):
    preds_all = []
    netGs.eval()
    with torch.no_grad():
        assert (x.shape[0] % models_num == 0)
        s = x.shape[0] // models_num
        for i in range(models_num):
            batch = x[i * s: (i + 1) * s, :]
            generator = getattr(netGs, 'Gmodel_' + str(i))
            generated = torch.cat([generator(batch), batch], dim=1)
            preds_all.append(generated)
    netGs.train()
    data = torch.cat(preds_all, axis=0)
    return data


def train_generator_ensemble_step(netCs, netGs, optGs, data, data_b, alpha=0.01):
    losses = []
    for i in range(netGs.num_models):
        generator = getattr(netGs, 'Gmodel_' + str(i))
        critic = getattr(netCs, 'Cmodel_' + str(i))
        loss = train_generator_step(netGs, critic, generator, optGs[i], data, data_b, alpha)
        losses.append(loss)
    return np.array(losses)


def adversarial_loss(netGs, netC, netG, data, data_b, alpha):
    x, weight, dlls = data
    x_b, weight_b, dlls_b = data_b

    real_full_0 = torch.cat([dlls, x], dim=1)
    generated_1 = torch.cat([netG(x), x], dim=1)
    generated_2 = torch.cat([netG(x), x], dim=1)

    if alpha == 0:
        generator_loss = torch.mean((cramer_critic(netC, real_full_0, generated_2) -
                                     cramer_critic(netC, generated_1, generated_2)) * weight)
    else:
        generated_b = torch.cat([netG(x_b), x_b], dim=1)
        concated_b = get_set_predictions(x_b, netGs=netGs)

        generator_loss = torch.mean((cramer_critic(netC, real_full_0, generated_2) -
                                     cramer_critic(netC, generated_1, generated_2)) * weight -
                                    alpha * ensemble_penalty(netC, concated_b, generated_b) * weight_b)

    return real_full_0, generated_1, generated_2, generator_loss


def train_generator_step(netGs, netC, netG, optG, data, data_b, alpha):
    optG.zero_grad()
    netC.eval()
    netG.train()

    _, _, _, generator_loss = adversarial_loss(netGs, netC, netG, data, data_b, alpha)

    generator_loss.backward()
    optG.step()

    return generator_loss.item()


def train_critic_ensemble_step(netGs, netCs, optCs, data, data_b, lambda_pt, iteration, batch_size, alpha=0.01):
    losses = []
    for i in range(netCs.num_models):
        generator = getattr(netGs, 'Gmodel_' + str(i))
        critic = getattr(netCs, 'Cmodel_' + str(i))
        loss = train_critic_step(netGs, critic, generator, optCs[i], data, data_b,
                                 alpha, lambda_pt, iteration, batch_size)
        losses.append(loss)
    return np.array(losses)


def train_critic_step(netGs, netC, netG, optC, data, data_b, alpha, lambda_pt, iteration, batch_size):
    optC.zero_grad()
    netG.eval()
    netC.train()

    real_full_0, generated_1, generated_2, generator_loss = adversarial_loss(netGs, netC, netG, data, data_b, alpha)

    # GP
    gradient_penalty = calc_gradient_penalty(netC, real_full_0, generated_1, generated_2, batch_size)

    critic_loss = lambda_pt(iteration) * gradient_penalty - generator_loss
    critic_loss.backward()
    optC.step()

    return critic_loss.item()


def train(train_data, boundary_data, val_data, path, netGs, netCs, device, alpha=1,
          alpha_iter=300, batch_size=32, total_iter=int(1e5), optCs=None, optGs=None):
    train_loader = repeater(torch.utils.data.DataLoader(ParticleSet(train_data),
                                                        batch_size=batch_size,
                                                        shuffle=False,
                                                        pin_memory=True))
    boundary_loader = repeater(torch.utils.data.DataLoader(ParticleSet(boundary_data),
                                                           batch_size=batch_size,
                                                           shuffle=False,
                                                           pin_memory=True))
    val_loader = repeater(torch.utils.data.DataLoader(ParticleSet(val_data),
                                                      batch_size=N_VAL,
                                                      shuffle=False,
                                                      pin_memory=True))
    if optCs is None:
        optCs = [torch.optim.RMSprop(getattr(netCs, 'Cmodel_' + str(i)).parameters(), lr=2e-4) for i in range(netCs.num_models)]
    if optGs is None:
        optGs = [torch.optim.RMSprop(getattr(netGs, 'Gmodel_' + str(i)).parameters(), lr=2e-4) for i in range(netGs.num_models)]

    CRITIC_ITERATIONS_CONST = 15
    VALIDATION_INTERVAL = 100
    #lambda_pt = lambda i: 20 / np.pi * 2 * torch.atan(torch.tensor(i, dtype=torch.float32, device=device) / 1e4)
    lambda_pt = lambda_pt = lambda i: 15.

    pt = []
    for i in tqdm_notebook(range(total_iter)):
        pt.append(lambda_pt(i))

    for iteration in tqdm(range(total_iter)):
        netGs.train()
        netCs.train()
        for critic_iter in range(CRITIC_ITERATIONS_CONST):
            x, weight, dlls = [i.to(device) for i in next(train_loader)]
            x_b, weight_b, dlls_b = [i.to(device) for i in next(boundary_loader)]

            critic_loss = train_critic_ensemble_step(netGs, netCs, optCs, (x, weight, dlls), (x_b, weight_b, dlls_b),
                                                     lambda_pt, iteration,
                                                     alpha=alpha, batch_size=batch_size)

        x, weight, dlls = [i.to(device) for i in next(train_loader)]
        x_b, weight_b, dlls_b = [i.to(device) for i in next(boundary_loader)]

        if iteration % alpha_iter == 0:
            alpha /= 10
        generator_loss = train_generator_ensemble_step(netCs, netGs, optGs, (x, weight, dlls),
                                                       (x_b, weight_b, dlls_b), alpha)

        netGs.eval()
        netCs.eval()
        if iteration % VALIDATION_INTERVAL == 0:
            clear_output(False)
            print(f'{iteration} - generator_loss: {generator_loss.mean()} - critic_loss: {critic_loss.mean()}')
            print('skip plot')

            with torch.no_grad():
                x, weight, dlls = [i.to(device) for i in next(val_loader)]
                preds = netGs(x)
                fig, axes = plt.subplots(2, 5, figsize=(45, 15))
                for INDEX, ax in zip((0, 1, 2, 3, 4), axes.flatten()[:5]):
                    _, bins, _ = ax.hist(dlls[:, INDEX].cpu(),
                                         bins=100,
                                         label="data")
                    for i, p in enumerate(preds):
                        ax.hist(p[:, INDEX].cpu(),
                                bins=bins,
                                label=f"generated {i}",
                                alpha=0.5)
                    ax.legend()
                    ax.set_title(dll_columns[INDEX])

                plt.show()

        if iteration % VALIDATION_INTERVAL == 0:
            generators_pt = 'generators_without_weights.pt'
            critic_pt = 'critic_without_weights.pt'
            opt_dict = {f'{i}_optimizer_state_dict': optGs[i].state_dict() for i in range(netGs.num_models)}
            torch.save({**{
                'iteration': iteration,
                'model_state_dict': netGs.state_dict(),
                'loss': generator_loss.mean()
            }, **opt_dict}, os.path.join(path, generators_pt))

            opt_dict = {f'{i}_optimizer_state_dict': optCs[i].state_dict() for i in range(netCs.num_models)}
            torch.save({**{
                'iteration': iteration,
                'model_state_dict': netCs.state_dict(),
                'loss': critic_loss.mean()
            }, **opt_dict}, os.path.join(path, critic_pt))

            print('model saved')

