import torch
from torch import nn
from .params import *

device = 'cuda' if torch.cuda.is_available() else 'cpu'


def get_noise(batch_size, mean=0, std=1):
    return torch.zeros(batch_size, LATENT_DIMENSIONS).normal_(mean, std).to(device)


def init_weights(m):
    if type(m) == nn.Linear:
        torch.nn.init.xavier_uniform_(m.weight)
        m.bias.data.fill_(0.01)


class Generator(nn.Module):
    def __init__(self, hidden_size=128, depth=5, s=9):
        super(Generator, self).__init__()
        self.layers = nn.Sequential(
            nn.Linear(LATENT_DIMENSIONS + s - 1 - y_count, hidden_size),
            nn.ReLU(),
            *[nn.Sequential(nn.Linear(hidden_size, hidden_size), nn.LeakyReLU(0.05)) for _ in range(depth)],
            nn.Linear(hidden_size, y_count),
        )
        self.apply(init_weights)

    def forward(self, x, noise=None):
        return self.layers(torch.cat([x, get_noise(x.shape[0])], dim=1))


class GeneratorEnsemble(nn.Module):
    def __init__(self, num_models=5, hidden_size=128, depth=5, s=9):
        super(GeneratorEnsemble, self).__init__()
        self.num_models = num_models
        for i in range(self.num_models):
            model = Generator(hidden_size=hidden_size,
                              depth=depth, s=s)
            setattr(self, 'Gmodel_' + str(i), model)

    def forward(self, x, noise=None):
        preds = []
        for i in range(self.num_models):
            model = getattr(self, 'Gmodel_' + str(i))
            preds.append(model(x))
        preds = torch.stack(preds)
        return preds


class Critic(nn.Module):
    def __init__(self, hidden_size=128, depth=5, s=9):
        super(Critic, self).__init__()
        self.layers = nn.Sequential(
            nn.Linear(s - 1, hidden_size),
            nn.ReLU(),
            *[nn.Sequential(nn.Linear(hidden_size, hidden_size), nn.LeakyReLU(0.05)) for _ in range(depth)],
            nn.Linear(hidden_size, CRAMER_DIM),
        )
        self.apply(init_weights)

    def forward(self, x):
        return self.layers(x)


class CriticEnsemble(nn.Module):
    def __init__(self, num_models=5, hidden_size=128, depth=5):
        super(CriticEnsemble, self).__init__()
        self.num_models = num_models
        for i in range(self.num_models):
            model = Critic(hidden_size=hidden_size,
                                depth=depth)
            setattr(self, 'Cmodel_' + str(i), model)

    def forward(self, x, noise=None):
        preds = []
        for i in range(self.num_models):
            model = getattr(self, 'Cmodel_' + str(i))
            preds.append(model(x))
        preds = torch.stack(preds)
        return preds
