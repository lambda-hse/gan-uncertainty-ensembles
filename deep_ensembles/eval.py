from .rich_utils import model_testing
import torch
import math
import numpy as np
from .params import *
import pandas as pd
from scipy.stats import ks_2samp, norm, kstest
import matplotlib.pyplot as plt
import seaborn as sns
from .train import repeater, ParticleSet
SAMPLES_NUM = 10000
MODELS_NUM = 10
weights_col = 'probe_sWeight'

features_mapping = [
    ('Brunel_P', 'Brunel_P'),
    ('Brunel_ETA', 'Brunel_ETA'),
    ('nTracks_Brunel', 'nTracks_Brunel'),
    ('RichDLLe', 'predicted_RichDLLe'),
    ('RichDLLk', 'predicted_RichDLLk'),
    ('RichDLLmu', 'predicted_RichDLLmu'),
    ('RichDLLp', 'predicted_RichDLLp'),
    ('RichDLLbt', 'predicted_RichDLLbt'),
]

ks_features_mapping = [
    ('RichDLLe'      , 'predicted_RichDLLe' ),
    ('RichDLLk'      , 'predicted_RichDLLk' ),
    ('RichDLLmu'     , 'predicted_RichDLLmu'),
    ('RichDLLp'      , 'predicted_RichDLLp' ),
    ('RichDLLbt'     , 'predicted_RichDLLbt'),
]


def eval_model_by_classifier(df):
    params = dict(
        max_depth=5,
        n_estimators=100,
        learning_rate=0.01,
        min_child_weight=50,
        n_jobs=24
    )

    data_merged = model_testing.merge_dataframes(df)
    scores = model_testing.kfold_test(data_merged, params)
    return scores


def eval_model_by_ks(df):
    feats_real, feats_gen = [
        list(x) for x in zip(*ks_features_mapping)
    ]
    data_real = df[feats_real].values
    data_gen = df[feats_gen].values
    stats, pvals = [], []
    for i in range(len(feats_real)):
        stat, pval = ks_2samp(data_real[:, i], data_gen[:, i])
        stats.append(stat)
        pvals.append(pval)

    return stats, pvals


def get_point_predictions(x_point, netGs, samples_num=SAMPLES_NUM, batch_size=100, models_num=MODELS_NUM, verbose=True):
    batched_preds = [[] for _ in range(models_num)]
    with torch.no_grad():
        for i in range(math.ceil(samples_num / batch_size)):
            n = min(samples_num - i * batch_size, batch_size)
            batch = torch.from_numpy(np.array([np.array(x_point,)] * n)).cuda()
            if verbose: print(f'batch: {i + 1}/{math.ceil(samples_num / batch_size)}\tsize: {n}')
            _, _, preds = netGs(batch)
            for j in range(models_num):
                batched_preds[j].append(preds[j].cpu())

    preds = []
    for model_batched_preds in batched_preds:
        preds.append(np.row_stack(model_batched_preds))
    return np.array(preds)


def get_set_predictions(x, netGs, samples_num, batch_size=100, models_num=MODELS_NUM, verbose=True):
    batched_preds = [[] for _ in range(models_num)]
    with torch.no_grad():
        for i in range(math.ceil(samples_num / batch_size)):
            n = min(samples_num, (i + 1) * batch_size)
            batch = x[i * batch_size: n, :].cuda()
            if verbose: print(f'batch: {i + 1}/{math.ceil(samples_num / batch_size)}\tsize: {batch.shape}')
            _, _, preds = netGs(batch)
            for j in range(models_num):
                batched_preds[j].append(preds[j].cpu())

    preds = []
    for model_batched_preds in batched_preds:
        preds.append(np.row_stack(model_batched_preds))
    return np.array(preds)


def generate_df(feat, predict, target, scaler):
    pred_data = np.concatenate([predict, feat], axis=1)
    val_data = np.concatenate([target, feat], axis=1)

    pred_data_inversed = scaler.inverse_transform(pred_data)
    val_data_inversed = scaler.inverse_transform(val_data)

    cols = ['RichDLLe', 'RichDLLk', 'RichDLLmu', 'RichDLLp', 'RichDLLbt',
            'Brunel_P', 'Brunel_ETA', 'nTracks_Brunel']

    df = pd.DataFrame(val_data_inversed, columns=cols)
    for ind, col in enumerate(dll_columns):
        df["predicted_" + col] = pred_data_inversed[:, ind]
    df['probe_sWeight'] = 1
    return df


def count_uncertainty(x, scaler, netGs, eval_model, samples_num, models_num, verbose=False):
    assert(x.shape[0] % models_num == 0)
    ensemble_preds = get_set_predictions(x, netGs, samples_num=samples_num,
                                         models_num=models_num, verbose=verbose)
    test_models_preds = get_set_predictions(x, netGs, samples_num=samples_num * models_num,
                                            models_num=models_num, verbose=verbose)
    concated_ensemble_preds = np.concatenate(ensemble_preds, axis=0)

    scores = []
    for model_preds in test_models_preds:
        df = generate_df(feat=x.cpu()[:samples_num * models_num], predict=model_preds,
                         target=concated_ensemble_preds, scaler=scaler)
        score = eval_model(df)
        scores.append(score)
    return scores


def count_model_accuracy(x, scaler, dll, netGs, eval_model, samples_num):
    _, _, preds = netGs(x)
    scores = []
    for i in range(len(preds)):
        model_preds = torch.tensor(preds[i][:samples_num]).cpu()
        dll = torch.tensor(dll[:samples_num]).cpu()
        df = generate_df(feat=x[:samples_num].cpu(), predict=model_preds, target=dll, scaler=scaler)
        score = eval_model(df)
        scores.append(score)
    return scores


def viz_uncertainty_heatmap(data_val_s, netGs, device, vs=[-6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6]):
    res = []
    data = data_val_s.values
    test_ood = data[(data[:, 5] > -1) & (data[:, 5] > -1) & (data[:, 6] < 0) & (data[:, 6] < 0)]

    for delta_x in vs:
        res.append([])
        for delta_y in vs:
            d = test_ood.copy()
            d[:, 5] = d[:, 5] - delta_x
            d[:, 6] = d[:, 6] - delta_y

            data_ood_df = pd.DataFrame(data=d, columns=data_val_s.columns)
            ood_loader = repeater(torch.utils.data.DataLoader(ParticleSet(data_ood_df),
                                                              batch_size=N_VAL,
                                                              shuffle=False,
                                                              pin_memory=True))
            x, weight, dlls = [i.to(device) for i in next(ood_loader)]
            unc = count_uncertainty(x, weight, netGs)
            res[-1].append(unc)

    d = []
    for x in res:
        d.append([])
        for y in x:
            d[-1].append(y[0][1])
    ax = sns.heatmap(np.array(d), yticklabels=vs, xticklabels=vs)
    ax.set(xlabel='Brunel_P delta', ylabel='Brunel_ETA delta')
    plt.show()